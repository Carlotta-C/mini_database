#include <stdio.h>
#include <stdlib.h>
#include "database.h"
#include <string.h> 

int main() {
    Database* d = NULL;

    Persona* p1 = (Persona*)malloc(sizeof(Persona));
    Persona* p2 = (Persona*)malloc(sizeof(Persona));
    Persona* p3 = (Persona*)malloc(sizeof(Persona));
    Persona* p4 = (Persona*)malloc(sizeof(Persona));

    strcpy(p1->name, "Ciak");
    strcpy(p1->surname, "Beans");
    strcpy(p1->address, "via dei fiori");
    p1->age = 46;

    strcpy(p2->name, "LILLI");
    strcpy(p2->surname, "PRISCI");
    strcpy(p2->address, "piazza dante");
    p2->age = 22;

    strcpy(p3->name, "Sara");
    strcpy(p3->surname, "Lavvi");
    strcpy(p3->address, "via delle mimose");
    p3->age = 82;

    strcpy(p4->name, "Pippi");
    strcpy(p4->surname, "Calzelunghe");
    strcpy(p4->address, "Foresta del colle");
    p4->age = 2;

    d = insert(d, p1);
    d = insert(d, p2);
    d = insert(d, p3);
    d = insert(d, p4);

    print_tree_str(d->name);
    print_tree_str(d->surname);
    print_tree_str(d->address);
    print_tree_int(d->age);

    printf("___Find by name___\n");
    Persona* pFind = findByName(d, "Aladin");
    printf("___Find by surname___\n");
    Persona* pFind1 = findBySurname(d, "Silia");
    printf("___Find by address___\n");
    Persona* pFind2 = findByAddress(d, "Hotel");
    printf("___Find by age___\n");
    Persona* pFind3 = findByAge(d, 21);

    return 0;
}

