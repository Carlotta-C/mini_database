#include "database.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

// Funzione ausiliaria per inserire un nodo nell'indice di tipo stringa
IndexNodeString* insertNodeString(IndexNodeString* root, char* value) {
    if (root == NULL) {
        IndexNodeString* newNode = (IndexNodeString*)malloc(sizeof(IndexNodeString));
        newNode->value = strdup(value); // Copia la stringa value in un nuovo buffer di memoria
        newNode->left = NULL;
        newNode->right = NULL;
        return newNode;
    }

    int cmp = strcmp(value, root->value);

    if (cmp < 0) {
        root->left = insertNodeString(root->left, value); // Inserisce il nodo a sinistra
    } else if (cmp > 0) {
        root->right = insertNodeString(root->right, value); // Inserisce il nodo a destra
    }

    return root;
}

// Funzione ausiliaria per cercare un nodo nell'indice di tipo stringa
Persona* findNodeString(IndexNodeString* root, char* value) {
    while (root != NULL) {
        int cmp = strcmp(value, root->value);
        if (cmp == 0) {
            // Nodo trovato, restituisce il riferimento alla persona associata al nodo
            return (Persona*)root;
        } else if (cmp < 0) {
            root = root->left; // Si sposta a sinistra nel sottoalbero
        } else {
            root = root->right; // Si sposta a destra nel sottoalbero
        }
    }

    // Nodo non trovato
    return NULL;
}

// Funzione ausiliaria per inserire un nodo nell'indice di tipo intero (INDEX)
IndexNodeInt* insertNodeInt(IndexNodeInt* root, int value) {
    if (root == NULL) {
        IndexNodeInt* newNode = (IndexNodeInt*)malloc(sizeof(IndexNodeInt));
        newNode->value = value;
        newNode->left = NULL;
        newNode->right = NULL;
        return newNode;
    }

    if (value < root->value) {
        root->left = insertNodeInt(root->left, value); // Inserisce il nodo a sinistra
    } else if (value > root->value) {
        root->right = insertNodeInt(root->right, value); // Inserisce il nodo a destra
    }

    return root;
}

// Funzione ausiliaria per cercare un nodo nell'indice di tipo intero (PERSONA)
Persona* findNodeInt(IndexNodeInt* root, int value) {
    while (root != NULL) {
        if (value == root->value) {
            // Nodo trovato, restituisce il riferimento alla persona associata al nodo
            return (Persona*)root;
        } else if (value < root->value) {
            root = root->left; // Si sposta a sinistra nel sottoalbero
        } else {
            root = root->right; // Si sposta a destra nel sottoalbero
        }
    }

    // Nodo non trovato
    return NULL;
}

// Funzione per inserire una persona nel database
Database* insert(Database* database, Persona* persona) {
    if (database == NULL) {
        return NULL; // Database non inizializzato
    }

    // Inserisce la persona nei diversi indici
    database->name = insertNodeString(database->name, persona->name);
    database->surname = insertNodeString(database->surname, persona->surname);
    database->address = insertNodeString(database->address, persona->address);
    database->age = insertNodeInt(database->age, persona->age);

    return database;
}

// Funzione per trovare una persona per nome nel database
Persona* findByName(Database* database, char* name) {
    if (database == NULL) {
        return NULL; // Database non inizializzato
    }

    return findNodeString(database->name, name);
}

// Funzione per trovare una persona per cognome nel database
Persona* findBySurname(Database* database, char* surname) {
    if (database == NULL) {
        return NULL; // Database non inizializzato
    }

    return findNodeString(database->surname, surname);
}

// Funzione per trovare una persona per indirizzo nel database
Persona* findByAddress(Database* database, char* address) {
    if (database == NULL) {
        return NULL; // Database non inizializzato
    }

    return findNodeString(database->address, address);
}

// Funzione per trovare una persona per et� nel database
Persona* findByAge(Database* database, int age) {
    if (database == NULL) {
        return NULL; // Database non inizializzato
    }

    return findNodeInt(database->age, age);
}

// Funzione ausiliaria per stampare gli elementi di un indice di tipo stringa (inorder traversal)
void print_tree_str(IndexNodeString* root) {
    if (root != NULL) {
        print_tree_str(root->left); // Visita il sottoalbero sinistro
        printf("%s\n", root->value); // Stampa il valore del nodo corrente
        print_tree_str(root->right); // Visita il sottoalbero destro
    }
}

// Funzione ausiliaria per stampare gli elementi di un indice di tipo intero (inorder traversal)
void print_tree_int(IndexNodeInt* root) {
    if (root != NULL) {
        print_tree_int(root->left); // Visita il sottoalbero sinistro
        printf("%d\n", root->value); // Stampa il valore del nodo corrente
        print_tree_int(root->right); // Visita il sottoalbero destro
    }
}

